//
//  character.swift
//  p3
//
//  Created by Dado on 3/16/20.
//  Copyright © 2020 Dado. All rights reserved.
//

import Foundation
class  Character  {
    //variables
    var name : String
    var lifePoint : Int
    var strength : Weapon
    static var allCharacter : [String : [Character]] = [:]
    static var character1 :Character = Character(name: "", lifePoint: 0, strength: Weapon(damage: 5))
    static var character2 :Character = Character(name: "", lifePoint: 0, strength: Weapon(damage: 5))
    //variables initialization
    init(name : String , lifePoint : Int , strength : Weapon  ) {
        self.name = name
        self.lifePoint = lifePoint
        self.strength = strength
    }
    //choose a character that belongs to player1's table
    static  func chooseCharacterPlayer1(){
        let player1Character = (([Character.allCharacter[Player.allPlayer[0].name]!])[0])
        if Game.indexPlayer == 0 {
            print(" ~~~~~~~~~❢❢❢~~~~~~~~~❢❢❢ 🟢choose the attacker 🤺 🟢 ❢❢❢~~~~~~~~~❢❢❢~~~~~~~~~")
        }else{
            print(" ~~~~~~~~~❢❢❢~~~~~~~~~❢❢❢ 🔴choose the adversary 🤺 🔴 ❢❢❢~~~~~~~~~❢❢❢~~~~~~~~~")
        }
        for i in 0..<player1Character.count{
            if player1Character[i].lifePoint != 0 {
                print("\n\(i+1))"
                    + "\n Character Name ⚜️ : \(player1Character[i].name)"
                    + "\n Life Point    ⏳  : \(player1Character[i].lifePoint) 💚")
            }
        }
        if let choice = readLine() {
            switch choice {
            case "1" :
                character1 = player1Character[0]
            case "2" :
                character1 = player1Character[1]
            case "3" :
                character1 = player1Character[2]
            default:
                print("I don't undestand")
            }
        }
    }
    //choose a character that belongs to player2's table
    static  func chooseCharacterPlayer2(){
        let player2Character = (([Character.allCharacter[Player.allPlayer[1].name]!])[0])
        if Game.indexPlayer == 1 {
            print(" ~~~~~~~~~❢❢❢~~~~~~~~~❢❢❢ 🟢choose the attacker 🤺 🟢 ❢❢❢~~~~~~~~~❢❢❢~~~~~~~~~")
        }else{
            print(" ~~~~~~~~~❢❢❢~~~~~~~~~❢❢❢ 🔴choose the adversary 🤺 🔴 ❢❢❢~~~~~~~~~❢❢❢~~~~~~~~~")
        }
        for i in 0..<player2Character.count{
            if player2Character[i].lifePoint != 0 {
                print("\n\(i+1))"
                    + "\n Character Name ⚜️ : \(player2Character[i].name)"
                    + "\n Life Point    ⏳  : \(player2Character[i].lifePoint) 💚")
            }
        }
        if let choice = readLine() {
            switch choice {
            case "1" :
                character2 = player2Character[0]
            case "2" :
                character2 = player2Character[1]
            case "3" :
                character2 = player2Character[2]
            default:
                print("I don't undestand")
            }
        }
    }
    static  func chooseCharacter(){
        chooseCharacterPlayer1()
        chooseCharacterPlayer2()
    }
    static func combat(attacker: Character , adversary : Character){
        if attacker.strength.damage < adversary.lifePoint{
            adversary.lifePoint -= attacker.strength.damage
        }else{
            adversary.lifePoint = 0
        }
    }
    // check if the fight is over
    static func checkingFight (playerIndex : Int) -> Bool{
        var  result = true
        let forkey = ((([Character.allCharacter[Player.allPlayer[playerIndex].name]!])[0]))
        if forkey[0].lifePoint != 0 || forkey[1].lifePoint != 0 || forkey[2].lifePoint != 0  {
            result = true
        }else{
            result = false
        }
        return result
    }
    //function to check if the name doesn't already exist
    static  func checking(characterName: String) -> Bool {
        var result = true
        for i in 0..<Game.CharactersNameList.count {
            if characterName == Game.CharactersNameList[i] {
                result = false
                print("Character name \(Game.CharactersNameList[i]) already exist choose another one")
            }
        }
        return result
    }
}
// Class Warrior
class Warrior : Character {
    static  var warrior : Warrior = Warrior(name: "", lifePoint: 0,strength: Weapon(damage: 0))
    static func createCharacter(){
        var check :Bool
        check = false
        //do while the character's name already exist
        while check == false{
            print("⚜️ ⚪️ Character name : ")
            let characterResponse = readLine()!
            Weapon.chooseWeapon()
            warrior = Warrior(name: characterResponse, lifePoint: 20, strength: Weapon.selectedWeapon)
            check = Character.checking(characterName: characterResponse)
        }
        Game.CharactersNameList.append(Warrior.warrior.name)
    }
}

// Class Dwarf
class Dwarf: Character {
    static  var dwarf : Dwarf = Dwarf(name: "", lifePoint: 0,strength: Weapon(damage: 0))
    static func createCharacter(){
        var check :Bool
        check = false
        //do while the character's name already exist
        while check == false{
            print("⚜️ ⚪️ Character name : ")
            let characterResponse = readLine()!
            Weapon.chooseWeapon()
            dwarf = Dwarf(name: characterResponse, lifePoint: 18, strength: Weapon.selectedWeapon)
            check = Character.checking(characterName: characterResponse)
        }
        Game.CharactersNameList.append(Dwarf.dwarf.name)
    }
}

// Class Colossus
class Colossus: Character {
    static  var colossus : Colossus = Colossus(name: "", lifePoint: 0,strength: Weapon(damage: 0))
    static func createCharacter(){
        var check :Bool
        check = false
        //do while the character's name already exist
        while check == false{
            print("⚜️ ⚪️ Character name : ")
            let characterResponse = readLine()!
            Weapon.chooseWeapon()
            colossus = Colossus(name: characterResponse, lifePoint: 16, strength: Weapon.selectedWeapon)
            check = Character.checking(characterName: characterResponse)
        }
        Game.CharactersNameList.append(Colossus.colossus.name)
    }
}
// Class Magus
class Magus: Character {
    static  var magus : Magus = Magus(name: "", lifePoint: 0,strength: Weapon(damage: 0))
    static func createCharacter(){
        var check :Bool
        check = false
        //do while the character's name already exist
        while check == false{
            print("⚜️ ⚪️ Character name : ")
            let characterResponse = readLine()!
            Weapon.chooseWeapon()
            magus = Magus(name: characterResponse, lifePoint: 18, strength: Weapon.selectedWeapon)
            check = Character.checking(characterName: characterResponse)
        }
        Game.CharactersNameList.append(Magus.magus.name)
    }
}
