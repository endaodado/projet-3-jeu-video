//
//  game.swift
//  p3
//
//  Created by Dado on 3/16/20.
//  Copyright © 2020 Dado. All rights reserved.
//

import Foundation
class Game {
    static var CharactersNameList :[String] = []
    static var gameOver = false
    static var indexPlayer = 0
    static var turnsNumber = 0
    static var surpriseBoxIndex = Int.random(in: 1...5)
    //FUNCTION TO START A GAME
    static func startGame(){
        //game unitialisation
        print("••••••••••••••••••••♨️🎮 WELCOME 🎮♨️••••••••••••••••••••••••"
            + "\n1.PLAY ➡️"
            + "\n2.EXIT ⬅️")
        if let choice = readLine() {
            switch choice {
            case "1" :
                for i in 1...2{
                    if i == 1{
                        print("First Player Name 👩🏼‍💻: ")
                    }else{
                        print("Second Player Name 👩🏼‍💻: ")
                    }
                    Player.createPlayer(newPlayer: Player(name: readLine()!))
                    Game.createTeam()
                }
                while gameOver == false {
                    Game.startBattle()
                    turnsNumber += 1
                }
            case "2" :
                print("bye bye 🤙🏾")
            default:
                print("I don't undestand")
            }
        }
    }
    //FUNCTION TO CREATE A TEAM
    static func createTeam()  {
        var playerName :String = ""
        var characterTable :[Character] = []
        //get the player name
        playerName = Player.allPlayer[Player.connectedPlayerIndex].name
        while characterTable.count < 3 {
            // choose the type of character
            print(" 🏇🏼 Chose the type of character 🤾🏼‍♀️ \n \n1) 🅦 Warrior  ★★★★★ \n \n2) 🅓 Dwarf    ★★★★☆ \n \n3) 🅒 Colossus ★★★☆☆ \n \n4) 🅜 Magus    ★★☆☆☆")
            let response = readLine()!
            switch response {
            case "1":
                Warrior.createCharacter()
                // Save a character in a table
                characterTable.append(Warrior.warrior)
                //Update allCharacterTable
                Character.allCharacter.updateValue(characterTable, forKey: playerName)
            case "2":
                Dwarf.createCharacter()
                // Save a character in a table
                characterTable.append(Dwarf.dwarf)
                //Update allCharacterTable
                Character.allCharacter.updateValue(characterTable, forKey: playerName)
            case "3":
                Colossus.createCharacter()
                // Save a character in a table
                characterTable.append(Colossus.colossus)
                //Update allCharacterTable
                Character.allCharacter.updateValue(characterTable, forKey: playerName)
            case "4":
                Magus.createCharacter()
                // Save a character in a table
                characterTable.append(Magus.magus)
                //Update allCharacterTable
                Character.allCharacter.updateValue(characterTable, forKey: playerName)
            default:
                print("I don't undestand")
            }
        }
    }
    
    //FUNCTION TO START A BATTLE
    static func startBattle(){
        print("it's \(Player.allPlayer[indexPlayer].name) 's turn")
        Weapon.supriseBox()
        // calling the choose a Character function of the Chararcter class
        Character.chooseCharacter()
        //        calling the surprise box function of the weapon class which will be executed only if the
        //        number of laps is equal to the index of the surprise box (random variable).
        Weapon.supriseBox()
        // calling  the action function of the character class
        if indexPlayer == 0{
            Character.combat(attacker: Character.character1, adversary: Character.character2)
            print("~~~~~~~~~❢❢❢~~~~~~~~~❢❢❢ \(Character.character1.name)🟢 attacked 🤼‍♂️ \(Character.character2.name)🔴❢❢❢~~~~~~~~~❢❢❢~~~~~~~~~"
                + " \n 🟢  : \(Character.character1.name) 〰️> \(Character.character1.lifePoint) 💚"
                + " \n 🔴  : \(Character.character2.name) 〰️> \(Character.character2.lifePoint) 💚 ")
            // check if the fight is over whit cheeking function of the  Character class
            if Character.checkingFight(playerIndex: 1) {
                indexPlayer = 1
            }else{
                Game.gameOver = true
                displayWinner()
            }
        }else {
            Character.combat(attacker: Character.character2, adversary: Character.character1)
            print("~~~~~~~~~❢❢❢~~~~~~~~~❢❢❢ \(Character.character2.name)🟢 attacked 🤼‍♂️ \(Character.character1.name)🔴❢❢❢~~~~~~~~~❢❢❢~~~~~~~~~"
                + " \n 🟢 : \(Character.character2.name) 〰️> \(Character.character1.lifePoint) 💚"
                + " \n 🔴 : \(Character.character1.name) 〰️> \(Character.character2.lifePoint) 💚 ")
            if Character.checkingFight(playerIndex: 0) {
                indexPlayer = 0
            }else{
                Game.gameOver = true
                displayWinner()
            }
        }
    }
    //FUNCTION TO DISPLAY A WINNER
    static func displayWinner(){
        print("🎊🎈 the winner is \(Player.allPlayer[indexPlayer].name) 🎉🎊"
            + "\n number of turns 🌀 ♻️ : \(turnsNumber)")
        print("\n The Character of the player  \(Player.allPlayer[0].name)  : ")
        for i in 0...2{
            let player1 = (([Character.allCharacter[Player.allPlayer[0].name]!])[0])
            print("\n ==================================================================="
                + "\n || Character Name ⚜️ : \(player1[i].name) || Life Point    ⏳  : \(player1[i].lifePoint) 💚 ||"
                + "\n ===================================================================")
        }
        print("\n The Character of the player  \(Player.allPlayer[1].name)  : ")
        for i in 0...2{
            let player2 = (([Character.allCharacter[Player.allPlayer[1].name]!])[0])
            print("\n ==================================================================="
                + "\n || Character Name ⚜️ : \(player2[i].name) || Life Point    ⏳  : \(player2[i].lifePoint) 💚 ||"
                + "\n ===================================================================")
        }
    }
}
