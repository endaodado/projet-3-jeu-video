//
//  player.swift
//  p3
//
//  Created by Dado on 3/16/20.
//  Copyright © 2020 Dado. All rights reserved.
//

import Foundation
class Player {
    var name : String
    static var connectedPlayerIndex = Player.allPlayer.startIndex
    static var allPlayer : [Player] = []
    //init function
    init(name : String) {
        self.name = name
    }
    // function to create a new player
    static func createPlayer(newPlayer : Player)  {
        Player.allPlayer.append(newPlayer)
        if allPlayer.count == 2{
            connectedPlayerIndex += 1
        }
        print("Hello \(Player.allPlayer[ connectedPlayerIndex].name) 😉 you can create your team 👊🏾 🤼!! \n")
    }
}
