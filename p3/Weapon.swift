//
//  weapon.swift
//  p3
//
//  Created by Dado on 3/16/20.
//  Copyright © 2020 Dado. All rights reserved.
//

import Foundation

// Class Weapon
class Weapon {
    var damage : Int
    static var selectedWeapon : Weapon = Weapon(damage: 0)
    static var damageRandom = Int.random(in: 2...15)
    init(damage : Int) {
        self.damage = damage
    }
    
    // Function to chose a weapon
    static func chooseWeapon(){
        print("\n ⚔️💣....choose a weapon for this character....💣⚔️ "
            + "\n"
            + "\n1) 🪓 Sword 🪓 "
            + "\n"
            + "\n2) 🗡 Axe 🗡")
        if let choice = readLine() {
            switch choice {
            case "1" :
                let  sword = Sword(damage: 15)
                selectedWeapon = sword
                
            case "2" :
                let   axe = Axe(damage: 10)
                selectedWeapon = axe
                
            default:
                print("I don't undestand")
            }
        }
    }
    static func supriseBox(){
        if Game.surpriseBoxIndex == Game.turnsNumber{
            let weapon = Weapon(damage: damageRandom)
            print("\n 🎊🎊🎊🎊🎊🎊🎊🎊🎊 \n 🎉Surpise Box 🎁 🎉 \n 🎊🎊🎊🎊🎊🎊🎊🎊🎊 \n weapon’s power : \(weapon.damage) 🔋")
            print("\n1) Use the weapon 🗃 \n2) Ignore ❌ ")
            if let choice = readLine(){
                switch choice {
                case "1":
                    if Game.indexPlayer == 0 && weapon.damage > Character.character1.strength.damage {
                        Character.character1.strength = weapon
                        print("✅ change of weapon carry out ✅")
                    }else if Game.indexPlayer == 0 && weapon.damage > Character.character2.strength.damage{
                        Character.character2.strength = weapon
                        print("✅ change of weapon carry out ✅")
                    }else{
                        print(" your weapon is more powerful ❌")
                    }
                case "2":
                    print("you lost the box 👻👋🏽")
                default:
                    print("unvailable choice")
                }
            }
        }
    }
}

// Class Sword
class Sword: Weapon {
    
}
// Class Axe
class Axe: Weapon {
}
